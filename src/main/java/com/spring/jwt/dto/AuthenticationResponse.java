package com.spring.jwt.dto;

import lombok.Data;

/**
 * ini adalah response jika user berhasil di autentikasi
 * ditandai dengan data balikannya adalah string Token
 */
@Data
public class AuthenticationResponse {

    private final String jwt;

    public AuthenticationResponse(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
